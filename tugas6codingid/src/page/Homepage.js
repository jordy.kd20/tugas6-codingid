import { BeachAccess, Image, Work } from "@mui/icons-material";
import { Avatar, Card, CardContent, CardHeader, CardMedia, Grid, List, ListItem, ListItemAvatar, ListItemText, Typography } from "@mui/material";
import { red } from '@mui/material/colors';
import axios from 'axios';
import React, { useEffect, useState } from 'react'

export const Homepage = () => {
    const [windowSize, setWindowSize] = useState(window.innerWidth);
  
    const [Pink, setPink] = useState(8)
    const [Brown, setBrown] = useState(4)
    const [Data, setData] = useState([]);
    
    
    const fetchdata = ()=>{
        try {
            const api = {
            method : "get",
            url: "http://localhost:3004/postgenerated",
            headers : {},
            };
            axios(api).then((value)=>{
            setData(value.data);
            });
            
        } catch (error) {
            console.log("Error nihh");
        }
        
    }

    console.log(windowSize);

    useEffect(()=>{
        fetchdata();
        const handleresize = ()=>{
            setWindowSize(window.innerWidth);
        };
        window.addEventListener('resize',handleresize);
        if(windowSize<900){
            setPink(12)
            setBrown(0)
        }
        else{
            setPink(8)
            setBrown(4)
        }
    },[windowSize,Pink,Brown]);
    console.log(Data);


  return (
    <div>
        <Grid container>
            <Grid item xs={Pink}>
                <Grid container >
                    {Data.map((item)=>{
                        return(
                            <Grid mt={2} item xl={6} sx={{justifyContent:'center'}}>
                                <Card sx={{ maxWidth: 400 }}>
                                <CardHeader
                                    avatar={
                                    <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                                        R
                                    </Avatar>
                                    }
                                    title={item.title}
                                    subheader={item.datePost}
                                />
                                <CardMedia
                                    component="img"
                                    height="400"
                                    image={item.img}
                                    alt="Paella dish"
                                />
                                <CardContent>
                                    <Typography variant="body2" color="text.secondary" sx={{fontFamily:'Helvetica'}} >
                                    {item.description}
                                    </Typography>
                                </CardContent>
                                </Card>
                            </Grid>
                        )
                    })}
                    
                </Grid>
            
            </Grid>
            <Grid item xs={Brown} sx={{display: { xs: 'none', sm: 'block' }}}>
            <List sx={{ bgcolor: 'background.paper' }}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <Image/>
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Photos" secondary="Jan 9, 2014" />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <Work />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Work" secondary="Jan 7, 2014" />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <BeachAccess />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Vacation" secondary="July 20, 2014" />
            </ListItem>
          </List>
            </Grid>
        </Grid>
    </div>
  )
}
