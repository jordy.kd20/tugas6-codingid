import { BrowserRouter as Router,Route } from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar';
import { Homepage } from './page/Homepage';


function App() {
  return (
    <Router>
        <Navbar/>
        <Route exact path="/">
          <Homepage/>
        </Route>
    </Router>
  );
}

export default App;
